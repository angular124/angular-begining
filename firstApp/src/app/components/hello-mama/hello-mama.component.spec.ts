import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HelloMamaComponent } from './hello-mama.component';

describe('HelloMamaComponent', () => {
  let component: HelloMamaComponent;
  let fixture: ComponentFixture<HelloMamaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HelloMamaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HelloMamaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
