import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})
export class PokemonListComponent implements OnInit {
    list:{name:string, id:number}[]=[];//listado vacio tipado
    @Input() textShow="Enviar";


  constructor() {
    console.log("0-Aqui solo la inyeccion de dependencias");

  }

  ngOnInit(): void {
      console.log("1-Start the component");
      this.list=[
        {
          name:"Pikachu",
          id:3,
        },
        {
          name:"Charmander",
          id:4,
        },
        {
          name:"Bulbasur",
          id:6,
        },
      ];
  };

}
