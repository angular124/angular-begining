import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonPractiseComponent } from './button-practise.component';

describe('ButtonPractiseComponent', () => {
  let component: ButtonPractiseComponent;
  let fixture: ComponentFixture<ButtonPractiseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ButtonPractiseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonPractiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
