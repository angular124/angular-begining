import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'firstApp';

  isActiveList=true;
  isActiveDetails=false;

  handleView(){
    this.isActiveList= !this.isActiveList;
  }
  
sayThings=()=>{
  console.log("Digo things");
}

}
